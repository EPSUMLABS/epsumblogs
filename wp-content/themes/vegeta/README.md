== Vegeta ==
Tested up to: WordPress 4.5
Version: 1.0.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Copyright (c) 2016 by Vegeta Theme (http://howlthemes.com/)
This Theme is distributed under the terms of the GNU GPL.

Vegeta is based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc.
Underscores is distributed under the terms of the GNU GPL v2 or later.

Supported browsers: Firefox, Opera, Chrome, Safari and IE9+ (Some css3 styles like shadows, rounder corners are not supported by IE8 and below).


== Description ==
An elegant, minimal & typography focused WordPress blog theme. This theme has powerful back-end which will help you customize it with ease.

For free themes support, please contact us http://howlthemes.com


== Copyright ==

Vegeta Theme bundles the following third-party resources:

FontAwesome, http://fontawesome.io/
Copyright (C) 2015, Dave Gandy,
Font Awesome licensed under SIL OFL 1.1, http://scripts.sil.org/OFL
Code licensed under MIT License, http://www.opensource.org/licenses/MIT

Sidr v1.2.1 - Copyright (c) 2013 Alberto Varela
License: MIT
Source: https://github.com/artberri/sidr

Image used in screenshot.png: 

A photo by Xuan Hoa Le (https://www.pexels.com/photo/city-vintage-filters-czech-republic-61381/), licensed under Creative Commons Zero(http://creativecommons.org/publicdomain/zero/1.0/)

A photo by Ed Gregory (http://stokpic.com/project/woman-working-on-laptop-from-hotel-bed/), licensed under Creative Commons Zero(http://creativecommons.org/publicdomain/zero/1.0/)
