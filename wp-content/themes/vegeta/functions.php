<?php
/**
 * howlthemes functions and definitions
 *
 * @package howlthemes
 */

if ( ! function_exists( 'vegeta_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function vegeta_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on howlthemes, use a find and replace
	 * to change 'howlthemes' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'vegeta', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'vegeta' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	
}
endif; // howlthemes_setup
add_action( 'after_setup_theme', 'vegeta_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vegeta_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'vegeta_content_width', 640 );
}
add_action( 'after_setup_theme', 'vegeta_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function vegeta_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'epsum labs' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	) );
}
add_action( 'widgets_init', 'vegeta_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function vegeta_scripts() {
	wp_enqueue_style( 'vegeta-style', get_stylesheet_uri() );

    $font1 = get_theme_mod('primary-font');
    if($font1 == null){
      $font1 = 'Roboto';
    }
    $font2 = get_theme_mod('secondary-font');
    if($font2 == null){
      $font2 = 'Roboto+Slab';
    }

  wp_enqueue_style( 'vegeta_google-fonts', '//fonts.googleapis.com/css?family='. str_replace(" ", "+", $font1) .':400,300,900|'. str_replace(" ", "+", $font2) . ':300');
  wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/css/font-awesome.min.css');
  wp_enqueue_script( 'vegeta_myscript', get_template_directory_uri().'/js/howljs.js', array( 'jquery' ), '', true);


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'vegeta_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom Excerpt Length
 */
function vegeta__excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'vegeta__excerpt_length', 999 );

/**
 * HowlFunctions
 */
require get_template_directory() . '/inc/howlfunction/functions2.php';

/**
 * Howl Social Buttons
 */
function vegeta_socialmediafollow(){
	if(get_theme_mod("fsocial_url") || get_theme_mod("tsocial_url") || get_theme_mod("gsocial_url") || get_theme_mod("psocial_url") || get_theme_mod("isocial_url") || get_theme_mod("ysocial_url") || get_theme_mod("rsocial_url") || get_theme_mod("tumsocial_url") || get_theme_mod("sondsocial_url") || get_theme_mod("lsocial_url")){
echo '<div class="drag-social-button"><h3 class="widget-title"><span>Follow Us</span></h3><ul>';
	}
   if(get_theme_mod("fsocial_url")){
    echo'<li><a class="fblinkbtn" href="'.esc_url(get_theme_mod("fsocial_url")).'" target="blank"><i class="fa fa-facebook"></i></a></li>';
}
   if(get_theme_mod("tsocial_url")){
echo'<li><a class="twlinkbtn" href="'.esc_url(get_theme_mod("tsocial_url")).'" target="blank"><i class="fa fa-twitter"></i></a></li>';
}
   if(get_theme_mod("gsocial_url")){
echo'
<li><a class="gplinkbtn" href="'.esc_url(get_theme_mod("gsocial_url")).'" target="blank"><i class="fa fa-google-plus"></i></a></li>';
}
if(get_theme_mod("isocial_url")){
echo'
 <li><a class="inslinkbtn" href="'.esc_url(get_theme_mod("isocial_url")).'" target="blank"><i class="fa fa-instagram"></i></a></li>';
}
if(get_theme_mod("psocial_url")){
echo'
 <li><a class="pilinkbtn" href="'.esc_url(get_theme_mod("psocial_url")).'" target="blank"><i class="fa fa-pinterest-p"></i></a></li>';
}
if(get_theme_mod("ysocial_url")){
echo' <li><a class="yolinkbtn" href="'.esc_url(get_theme_mod("ysocial_url")).'" target="blank"><i class="fa fa-youtube"></i></a></li>';
}
if(get_theme_mod("lsocial_url")){
echo'<li><a class="lilinkbtn" href="'.esc_url(get_theme_mod("lsocial_url")).'" target="blank"><i class="fa fa-linkedin"></i></a></li>';
}
if(get_theme_mod("rsocial_url")){
echo' <li><a class="rslinkbtn" href="'.esc_url(get_theme_mod("rsocial_url")).'" target="blank"><i class="fa fa-rss"></i></a></li>';
}
if(get_theme_mod("tumsocial_url")){
echo' <li><a class="tulinkbtn" href="'.esc_url(get_theme_mod("tumsocial_url")).'" target="blank"><i class="fa fa-tumblr"></i></a></li>';
}
if(get_theme_mod("sondsocial_url")){
echo' <li><a class="sndlinkbtn" href="'.esc_url(get_theme_mod("sondsocial_url")).'" target="blank"><i class="fa fa-soundcloud"></i></a></li>';
}
	if(get_theme_mod("fsocial_url") || get_theme_mod("tsocial_url") || get_theme_mod("gsocial_url") || get_theme_mod("psocial_url") || get_theme_mod("isocial_url") || get_theme_mod("ysocial_url") || get_theme_mod("rsocial_url") || get_theme_mod("tumsocial_url") || get_theme_mod("sondsocial_url") || get_theme_mod("lsocial_url")){
echo "</ul></div>";
	}
}
/*
 * Registering Footer Area
*/
function vegeta_fwidgets_init() {
  register_sidebar( array(
    'name'          => __( 'Footer', 'epsum blogs' ),
    'id'            => 'footer-1',
    'description'   => '',
    'before_widget' => '<aside id="%1$s" class="fwidget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h4 class="fwidget-title">',
    'after_title'   => '</h4>',
  ) );
}
add_action( 'widgets_init', 'vegeta_fwidgets_init' );
/*-----------------------
* Custom Logo Support
*----------------------*/
function vegeta_custom_logo() {
    add_theme_support('custom-logo');
}

add_action('after_setup_theme', 'vegeta_custom_logo');
