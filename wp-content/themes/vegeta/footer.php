<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package howlthemes
 */
?>

	</div><!-- #content -->

<?php  vegeta_threecolumnfooter(); ?>

<?php wp_footer(); ?>

</body>
</html>
